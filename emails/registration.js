const keys = require("../keys")

module.exports = function (email) {
  return {
    to: email,
    from: keys.EMAIL_FROM,
    subject: "Регистрация прошла успешно, аккаунт создан",
    html: `
        <h1>Здавствуйте!</h1>
        <p>Вы успешно создали аккаунт с e-mail - ${email}</p>
        <hr />
        <a href="${keys.BASE_URL} "></a>
    `,
  }
}
