const multer = require("multer")

const storage = multer.diskStorage({
  destination(req, file, cb) {
    console.log(file)
    console.log(req.file )
    cb(null, "images")
  },
  filename(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname)
  },
})

const allowedTypes = ["image/png", "image/jpg", "image/jpeg"]

const fileFilter = (req, file, cb) => {
  console.log('1',)
  if (allowedTypes.includes(file.mimetype)) {
    console.log('2',)
    cb(null, true)
  } else {
    console.log('3',)
    cb(null, false)
  }
}

module.exports = multer({
  storage,
  fileFilter,
})