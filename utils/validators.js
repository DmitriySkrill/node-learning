const { body } = require("express-validator")
const User = require("../models/user")

exports.registerValidators = [
  body("email")
    .isEmail()
    .withMessage("Введите корректный email")
    .custom(async (value, req) => {
      try {
        const user = await User.findOne({ email: value })
        if (user) {
          return Promise.reject("Такой email уже задан")
        }
      } catch (error) {
        console.log(error)
      }
    })
    .normalizeEmail(),
  body("password")
    .isLength({ min: 6, max: 56 })
    .withMessage("Пароль должен быть минимум 6 символов")
    .isAlphanumeric()
    .withMessage("не должно быть русских букв")
    .trim(),
  body("confirm")
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error("Пароли должны совпадать")
      }
      return true
    })
    .trim(),
  body("name")
    .isLength({ min: 3 })
    .withMessage("Имя должно быть не менее 3 символов")
    .trim(),
]

exports.courseValidators = [
  body("title")
    .isLength({ min: 3 })
    .withMessage("Минимальная длина названия 3 символа")
    .trim(),
  body("price").isNumeric().withMessage("Введите корректную цену"),
  body('img', 'Введите корректный url картинки').isURL()
]
